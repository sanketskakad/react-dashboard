import React from "react";
import Cards from "../../components/Cards/Cards";
import Table from "../../components/Table/Table";
import { Route, Routes } from "react-router-dom";
import "./MainDash.css";
import Updates from "../../components/Updates/Updates";
const MainDash = () => {
  return (
    <div className='MainDash'>
      <h1>Dashboard</h1>
      <Routes>
        <Route
          path='/'
          element={
            <>
              <Cards />
              <Table />
            </>
          }
        />

        <Route path='/orders' element={<Table />} />
        <Route
          path='/customers'
          element={
            <>
              <Updates />
            </>
          }
        />
      </Routes>
    </div>
  );
};

export default MainDash;
