import "./App.css";
import MainDash from "./pages/MainDash/MainDash";
import RightSide from "./components/RightSide/RightSide";
import Sidebar from "./components/Sidebar";
import { BrowserRouter } from "react-router-dom";

function App() {
  return (
    <div className='App'>
      <BrowserRouter>
        <div className='AppGlass'>
          <Sidebar />
          <MainDash />
          <RightSide />
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
